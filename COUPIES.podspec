Pod::Spec.new do |spec|
    spec.name                    = "COUPIES"
    spec.version                 = "1.11.0"
    spec.platform                = :ios, "8.0"
    spec.summary                 = "iOS SDK for integrating COUPIES coupons into your application."
    spec.homepage                = "https://gitlab.com/coupies/ios-sdk"
    spec.social_media_url        = "https://facebook.com/COUPIES"
    spec.authors                 = { "Felix Gillen" => "felix.gillen@coupiespec.de" }
    spec.license                 = { :type => "Copyright",
                                     :text => "Copyright 2019 COUPIES GmbH. All rights reserved." }
    spec.source                  = { :git => "https://gitlab.com/coupies/ios-sdk.git",
                                     :tag => spec.version.to_s }
    spec.resources               = "COUPIES.framework/Versions/A/Resources/*.bundle"
    spec.ios.vendored_frameworks = "COUPIES.framework"
    spec.swift_version		     = "5.0"
    spec.library                 = "iconv"
    spec.frameworks              = "AdSupport", "QuartzCore", "CoreAudio", "CoreMedia", "CoreImage","CoreVideo", "CoreGraphics", "AudioToolbox", "AVFoundation", "Foundation", "UIKit", "CoreGraphics", "CoreLocation", "Mapkit", "OpenGLES", "MobileCoreServices", "ImageIO", "GLKit" 
    spec.requires_arc            = true
    spec.xcconfig                = { "OTHER_LDFLAGS" => "$(inherited) -ObjC -all_load",
                                  "CLANG_MODULES_AUTOLINK" => "YES" }
    spec.dependency 'AMPopTip'
end
